var txtAddress = document.createElement("input");
txtAddress.setAttribute("id", "server");

var btn = document.createElement("button");
btn.innerText = "Quick Connect";
btn.setAttribute("onclick", "connect('ws://' + document.getElementById('server').value + ':443');setNick(document.getElementById('nick').value);");

var dialog = document.getElementById("helloDialog");
dialog.appendChild(txtAddress);
dialog.appendChild(btn);

// Available API of main_out:
//
//setNick=function(nick);
//setRegion=function(region);
//setSkins=function(skins);
//setNames=function(names);
//setDarkTheme=function(darkTheme);
//setColors=function(colors);
//setShowMass=function(showMass);
//connect=function(url);